"""add new fields to user table

Revision ID: 64fc3ab51e16
Revises: f05d37c27895
Create Date: 2022-07-16 22:38:32.035042

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "64fc3ab51e16"
down_revision = "f05d37c27895"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("users", sa.Column("first_name", sa.String(), nullable=False))
    op.add_column("users", sa.Column("last_name", sa.String(), nullable=False))
    op.add_column("users", sa.Column("patronymic", sa.String(), nullable=False))
    op.drop_index("ix_users_name", table_name="users")
    op.create_index(op.f("ix_users_first_name"), "users", ["first_name"], unique=False)
    op.drop_column("users", "name")
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "users", sa.Column("name", sa.VARCHAR(), autoincrement=False, nullable=False)
    )
    op.drop_index(op.f("ix_users_first_name"), table_name="users")
    op.create_index("ix_users_name", "users", ["name"], unique=False)
    op.drop_column("users", "patronymic")
    op.drop_column("users", "last_name")
    op.drop_column("users", "first_name")
    # ### end Alembic commands ###
