**Fully asynchronous FastAPI app.**

**Local setup**
You can also install FastAPI app locally using Poetry(`pip install poetry` if you does not have Poetry).
- `docker-compose up -d --build` - to run FastAPI app and Postgres using Docker(ignore that app runs too, next commands are required too.)
- `poetry config virtualenvs.in-project true` - create .venv in project directory
- `poetry shell` - creates and enters to Poetry virtual environment
- `poetry install` - installs all dependencies of FastAPI app (using `pyproject.toml` and `poetry.lock` files)
- `alembic upgrade head`
- `uvicorn src.app.main:app --host 127.0.0.1 --port 80 --reload`