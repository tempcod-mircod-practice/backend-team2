FROM python:3.10.0-slim

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONBUFFERED 1


RUN pip install poetry
COPY pyproject.toml poetry.lock ./

RUN poetry config virtualenvs.create false \
    && poetry install --no-dev --no-interaction --no-ansi

ARG CI_COMMIT_SHA
ARG CI_COMMIT_REF_NAME

ENV CI_COMMIT_SHA="${CI_COMMIT_SHA}"
ENV CI_COMMIT_REF_NAME="${CI_COMMIT_REF_NAME}"

COPY . .

CMD uvicorn src.app.main:app --host 0.0.0.0 --port 80 --reload